var toolbar;
var toolbarch ={
	toolbar0:"首页",
	toolbar1:"关于源质未来",
	toolbar2:"早期投资者",
	toolbar3:"发展计划",
	toolbar4:"社群",
	toolbar5:"产品",
	toolbar6:"English",
	toolbar7:"源质浏览器",
	toolbar8:"源质OS",
	toolbar9:"帮助",
	toolbar10:"源质浏览器",
	toolbar11:"源质OS",
	toolbar12:"帮助中心"
}
var toolbaren ={
	toolbar0:"HOME",
	toolbar1:"ABOUT",
	toolbar2:"Investor",
	toolbar3:"BP",
	toolbar4:"Community",
	toolbar5:"Products",
	toolbar6:"中文",
	toolbar7:"Browser",
	toolbar8:"OS",
	toolbar9:"Help",
	toolbar10:"ELET-Browser",
	toolbar11:"ELET-OS",
	toolbar12:"Help"
}
var home;
var homech ={
	download1:"Win下载",
	download2:"MAC下载",
	download3:"iOS下载",
	download4:"Android下载",
	tip:"即将上线,敬请期待!",
	home:"“价值互联网时代”生产力平台",
	title1:"白皮书下载",
	home1:"关于源质未来",
	home2:"源质未来项目是区块链分布式算力与流量生态系统，包括两个部分：源质浏览器、源质 OS ，同时支持个人和企业用户。",
	home3:"ELET是基于源质未来生态通证，其将应用于源质浏览器、源质OS、源质算力云等多种场景中，持有ELET可以同时获得来自互联网领域的流量红利",
	home4:"和区块链行业的算力分成。",
	title2:"我们的愿景",
	home5:"“信贷权即是人权。就是说，每个人都应该有获得金融服务机会的权利。只有每个人拥有金融服务的机会，才能让每个人有机会参与经济的发展，",
	home6:"才能实现社会的共同富裕，建立和谐社会与和谐世界。”",
	home7:"——穆罕默德·尤努斯（Muhammad Yunus）",
	home8:"区块链是人类发现“新大陆”的“哥伦布帆船”；哥伦布拓展了人类社会的物理空间；区块链将拓展人类社会的“数字空间“； ",
	home9:"数字空间与物理空间不是取代、颠覆的关系，它们是“平行宇宙”。 ",
	home10:"我们希望人人都能参与区块链早期建设，获得应有的权利。",
	title3:"产品结构",
	product1:"ELET是基于源质浏览器和源质OS发行Token。",
	product2:"源质浏览器",
	product3:"源质浏览器是一款基于区块链技术的浏览器。",
	product4:"源质OS",
	product5:"源质OS是支持多种平台的开放式企业级操作系统。",
	token1:"源质Token ELET由源质浏览器平台一次性生成，共计100亿个",
	token2:"未成功募集部分自动进入挖矿奖励池子",
	token3:"未来基金会运营部分收益也将进入奖励池",
	token4:"用于日常运转、生态奖励等",
	token5:"用于运营推广、初期平台搭建等",
	title4:"早期投资者",
	title5:"合作伙伴",
	title6:"发展计划",
	plan1:"发布源质浏览器V0.9 Beta 公测版",
	plan2:"移动端源质浏览器 IOS版上线",
	plan3:"发布源质浏览器Android版",
	plan4:"AI推荐区块链文章",
	plan5:"1.源质OS上线",
	plan6:"2.IM功能，支持telegram",
	plan7:"项目启动",
	plan8:"启动A轮融资",
	plan9:"源质浏览器用户量突破100万",
	plan10:"中原区块链、火币等专版浏览器上线",
	plan11:"源质MAC版上线",
	plan12:"北京数立方软件有限公司 All Rights Reserved 京ICP备17030519号-2 联系电话：010-59459449"
}
var homeen={
	download1:"Win",
	download2:"MAC",
	download3:"iOS",
	download4:"Android",
	tip:"Coming soon!",
	home:"Value Internet Productivity Platform",
	title1:"White paper",
	home1:"About the Elementium Future",
	home2:"The Elementium Future is a blockchain distributed computing power and traffic ecosystem, which consists of two parts: the Elementium Browser, the Elementium OS, and supports both individual and enterprise users.",
	home3:"ELET is based on the Elementium Future ecological pass, which will be applied to a variety of scenarios such as Elementium Browser, Elementium OS, Elementium Hashrate cloud, etc. Holding ELET can simultaneously obtain traffic dividends from the Internet domain.",
	home4:"And the division of computing power in the blockchain industry.",
	title2:"Our vision",
	home5:'"The right to credit is human rights. That is to say, everyone should have the right to access financial services. Only everyone has the opportunity to have financial services, so that everyone has the opportunity to participate in economic development.',
	home6:'Only in this way can we achieve common prosperity in society and build a harmonious society and a harmonious world."',
	home7:"- Muhammad Yunus",
	home8:'The blockchain is the "Columbus sailboat" that humans have discovered "New World"; Columbus has expanded the physical space of human society; the blockchain will expand the "digital space" of human society;',
	home9:'Digital space and physical space are not a substitute, subversive relationship, they are "parallel universe."',
	home10:"We hope that everyone can participate in the early construction of the blockchain and get the rights it deserves.",
	title3:"Product Structure",
	product1:"ELET is based on the Elementium Browser and the Elementium OS release Token.",
	product2:"Elementium Browser",
	product3:"Elementium Browser is a browser based on blockchain technology.",
	product4:"Elementium OS",
	product5:"Elementium OS is an open enterprise operating system that supports multiple platforms.",
	token1:"The Elementium Token (ELET) is generated once by the Elementium Browser platform, totaling 10 billion",
	token2:"Unsuccessfully recruited part of the automatic entry into the mining reward pool",
	token3:"Future fund operating part of the proceeds will also enter the reward pool",
	token4:"For daily operations, ecological rewards, etc.",
	token5:"Used for operation promotion, initial platform construction, etc.",
	title4:"Investor",
	title5:"Partners",
	title6:"Plans",
	plan1:"Release Elementium Browser V0.9 Beta public beta",
	plan2:"Release Elementium Browser iOS",
	plan3:"Release Elementium Browser Android",
	plan4:"AI recommended blockchain article",
	plan5:"1. Elementium OS goes online",
	plan6:"2.IM function, support telegram",
	plan7:"Project begining",
	plan8:"Start the pre-sales recruitment",
	plan9:"Users amount of Elementium Browser exceeded 1 million",
	plan10:"Launch Central blockchain, Huobi and other OEM edition browsers",
	plan11:"Release Elementium MAC",
	plan12:"Beijing Digital Cube Software Co., Ltd. All Rights Reserved"
}
var Elementium;
var Elementiumch ={
	tip1:"敬请期待",
	browser1:"源质浏览器",
	browser2:"会赚钱的浏览器",
	browser3:"M63极速内核",
	browser4:"智能挖矿",
	browser5:"资讯网罗",
	browser6:"丰富插件",
	browser7:"网址导航",
	elet1:"关于ELET",
	elet2:"什么是ELET？",
	elet3:"源质token简称ELET，是去中心化的区块链数字资产，是基于源质未来生态而发行的Token。",
	elet4:"如何获得ELET？",
	elet5:"ELET目前主要有三种获取方式：",
	elet6:"1.使用源质浏览器进行挖矿，根据用户设备配置高低产出ELET。",
	elet7:"2.用户奖励获得，项目初期，基金会将拿出ELET总量的40%设立奖励池，用以激励用户。在今后的运营活动中，源质会陆续对活跃用户进行ELET奖励。",
	elet8:"3.交易获得，任何用户均可以通过交易获取ELET。未来ELET将会在各大交易所挂牌，用户可以根据自己的实际需求在交易所购买ELET。",
	elet9:"ELET有什么用？",
	elet10:"ELET将作为支付工具应用于多个场景中，未来势必会吸引更为广大的用户群体。",
	elet11:"源质生态内部流通：可购买广告位，分布式账本节点，优化项目展示，源质商城消费。",
	elet12:"持有ELET奖励：有宣发需求的Token发行方可根据ELET持有数量向用户进行空投。",
	elet13:"参与平台重大决策：平台利润分配方案变更、基金会组成变更、新业务拓展等重大事项需要全体ELET持有者进行投票表决，表决权重与持有ELET数量占流通ELET数量比重一致。",
	os1:"源质OS",
	os2:"源质OS是为区块链设计的轻量级操作系统，完美适配x86及ARM架构下的主流硬件设备，秉承了源质浏览器快速、简洁、安全的特性。源质OS对区块链中分布式数据存储、点对点传输、共识机制、加密算法等核心技术提供高效的底层支持，并可以从系统层面对这些核心算法进行深度包装，无缝连接线上主链与用户物理设备，真正做到让区块链技术连接一切。同时任意两台装有源质浏览器或源质OS的设备可以瞬间进行数据同步。轻量的系统设计会降低电量消耗，提高移动设备的续航能力。"
};
var Elementiumen ={
	tip1:"Coming soon",
	browser1:"Elementium Browser",
	browser2:"The browser can make money.",
	browser3:"M63 fast kernel",
	browser4:"Intelligent mining",
	browser5:"Information network",
	browser6:"Lots of Extensions",
	browser7:"Websites Guide",
	elet1:"About ELET",
	elet2:"What's ELET?",
	elet3:"ELET as a decentralized Blockchain digital asset is the Tokens issued based on Elementium Future Ecology.",
	elet4:"How to obtain ELET?",
	elet5:"ELET currently has three main acquisition methods:",
	elet6:"1.  Obtaining ELET from mining, the amount related to equipment performance.",
	elet7:"2. Obtaining from rewards for users. In the early stage of the project, the Foundation will contribute 40% of total ELET as reward pool to motivate users. In the future, it will put great profits into the reward pool.",
	elet8:"3. Obtaining from transaction. Any user can obtain the ELET through the transaction. In the future, ELET will be listed on major exchanges, and users can purchase ELET on the exchange according to their actual needs.",
	elet9:"Use of ELET",
	elet10:"Because of its unique characteristic of acquisition of profit, it will entail attracting more and more users in the future, based on which, ELET is about to be applied into several scenarios as payment tool.",
	elet11:"Elementium Ecology Internal Circulation: You can purchase advertising space, distributed ledger nodes, optimize project display, and Elemtnium shopping mall consumption.",
	elet12:"Holding an ELET reward: A Token issuer with a declared demand can make an airdrop to the user based on the number of ELET holdings.",
	elet13:"Participate in major decision-making on the platform: major changes in platform profit distribution plan, change in foundation composition, new business development, etc.The holder of the ELET is voting, and the voting weight is the same as the number of holding ELET in the number of circulating ELET.",
	os1:"Elenmentium OS",
	os2:"The Elenmentium OS is a lightweight operating system designed for the blockchain. It is perfectly adapted to the mainstream hardware devices under the x86 and ARM architectures. It inherits the features of the Elenmentium browser, which is fast, simple and secure. The Elenmentium OS provides efficient underlying support for core technologies such as distributed data storage, point-to-point transmission, consensus mechanism, and encryption algorithms in the blockchain, and can deeply package these core algorithms from the system layer, seamlessly connecting the online backbone. With the user's physical device, truly let the blockchain technology connect everything. At the same time, any two devices equipped with Elenmentium browser or Elenmentium OS can synchronize data in an instant. Lightweight system design reduces power consumption and improves the endurance of mobile devices."
};

var faq;
var faqCN = {
	 navtitle1:'使用说明',
	 navtext1:"下载与安装",
	 navtext2:"注册",
	 navtext3:"挖矿功能",
	 navtext4:"个人中心",
	 navtitle2:"问题分类",
	 navtext5:"常见问题",
	 navtext6:"软件使用",
	 navtext7:"收益与提现",
	 navtext8:"更多问题",
	 instructor:"浏览器使用说明",
	 instructorText:"源质浏览器是全球首家真实挖矿浏览器，最低支持64位操作系统、具备独立显卡的计算机进行挖矿（数字货币获取）。",
	 download:"下载与安装",
	 downloadText1:'打开源质未来官网',
	 downloadText11:"选择客户端类型进行下载。",
	 downloadText2:"*目前支持PC端、ios端下载，Android与Mac近期即将上线。",
	 downloadText3:"下载完成后，点击【一键安装】即可。",
	 register:"注册",
	 register1:"1.正常安装后，打开源质浏览器",
	 register11:"点击右上角图标进行注册。",
	 register2:"或者直接点击",
	 register21:"进入注册页面。",
	 register3:"2.根据提示，填写相关信息，完成后点击【注册】",
	 mining:"挖矿功能",
	 mining1:"1.进入挖矿页面",
	 mining2:"点击右上角的矿锄图标进入挖矿页面",
	 mining3:"2.登录",
	 mining4:"点击右上角的登录，输入注册好的账号与密码进行登录",
	 mining5:"3.算力池",
	 mining6:"点击开始挖矿即可一键智能挖矿，在左侧可以看到挖矿的相关数据与收益。",
	 mining7:"实时算力:",
	 mining71:"与电脑配置有关，较好的电脑配置会获取更多算力。",
	 mining8:"已确认收益:",
	 mining81:"已经确认结算，进入账户的收益。",
	 profit:"待确认收益：",
	 profitTex:"该数值为当前算力的预估收益，属尚未结算收益",
	 protext2:"在每日结算后会添加到已确认收益中。（实际收益以“已确认收益为准）",
	 protext3:"下方的币种列表可以看到该设备可挖币种的价格与涨幅。",
	 balanceTile:"4.配置与我的钱包",
	 balance1:"开始挖矿后，在配置中可以查看设备相关情况。",
	 balance2:"可以通过设置菜单进行挖矿效率调节，可以一边玩游戏看电影，一边挖矿赚钱。",
	 balance3:"在我的钱包中，能查看到该设备所挖到的所有币种、收益、上次结算时间。",
	 myaccount:"个人中心",
	 enteraccount:"1.进入个人中心",
	 enteraccounttext1:"登录状态下，点击左上角的图标",
	 enteraccounttext12:"即可进入个人中心。如下图：",
	 myassets:"2.我的资产与我的设备",
	 myassettext1:"进入个人中心可以看到我的资产与我的设备。",
	 myassettext2:"我的资产",
	 myassettext3:"币种：所有收益的币种类型。",
	 myassettext4:"累计总收益：当前账号所有设备挖矿收益总和。",
	 myassettext5:"可用：可以进行提现的收益。",
	 myassettext6:"冻结：正在进行提现的收益。",
	 myassettext7:"已提现：已经提现成功的收益数值。",
	 myassettext8:"我的设备：源质浏览器一个账号可以使用多台设备挖矿。",
	 myassettext9:"在我的设备中可以看到所有挖矿设备的具体状态，并且能够分类查询。",
	 withdrawal:"提币须知：",
	 withdrawaltext1:"1. 提币一般1-3个工作日到账。",
	 withdrawaltext2:"2. 在活动期间内，提币到指定交易所无门槛与手续费。",
	 withdrawaltext3:"若有疑问请添加客服QQ：1836228446。",
	 questionType:"源质浏览器问题分类",
	 commonques:"常见问题",
	 commonques1:"一、源质浏览器与普通浏览器的区别？",
	 commonques1text:"区别于普通浏览器，源质浏览器是全球首家真实挖矿浏览器，除了基本的极速浏览功能外，还具备智能挖矿和区块链优质资讯集合功能。",
	 commonques2:"二、什么是挖矿？会得到什么？",
	 commonques1text1:"挖矿可以理解为使用电脑中的CPU、GPU等资源进行数字货币获取，性能越好，挖矿收益越高。",
	 commonques1text2:"获得的数字货币可以折换成法定货币，而源质浏览器为您提供一键智能挖矿的功能。",
	 commonques3:"三、什么电脑可以使用源质浏览器挖矿？",
	 commonques3text:"源质浏览器最低支持64位操作系统、独立显卡的电脑使用挖矿功能。",
	 commonques4:"四、源质浏览器现有什么种类的客户端？",
	 commonques4text:"源质浏览器目前PC端、ios端已经开放下载，Android与Mac近期即将上线。",
	 commonques5:"五、当前支持挖矿币种都有哪些？",
	 commonques5text:"源质浏览器支持以太坊（ETH）与门罗币（XMR）挖取，陆续会根据挖取效率与收益添加新币种。",
	 commonques6:"六、挖矿每天能赚多少钱？",
	 commonques6text:"挖矿赚取的收益与您电脑配置和币价浮动有关，一般中等配置电脑每天挂机挖矿收益约4元左右，配置越高收益越好。币价上涨也会让您的收益提升。除此之外，每日使用源质浏览器并参加相关活动还可获得ELET奖励。",
	 commonques7:"七、ELET是什么？",
	 commonques7text:"ELET是去中心化的区块链数字资产，是基于源质未来生态而发行的Token，其将应用于源质浏览器、源质OS的多种场景中。",
	 commonques8:"八、ELET的作用？",
	 commonques8text:"ELET是一种稳定且价值不断提升的数字资产，可在各类交易所中购买与出售，并将作为支付工具，应用于源质浏览器及源质OS等多种场景中。",
	 commonques9:"九、ELET获得途径？",
	 commonques9text:"ELET目前可以通过交易所购买或使用源质浏览器挖矿及参加相关活动获取。",
	 commonques10:"十、ELET的官方交流群都有哪些？",
	 commonques10text3:"微信群：Elementium_ELET",
	 commonques11:"十一、为何众多矿工使用源质浏览器挖矿？",
	 commonques11text1:"1、使用简单：只需注册登录，即可进行一键挖矿，无需任何复杂设置。",
	 commonques11text2:"2、收益最优：源质智能挖矿根据本机配置和机主需求，自动选择最高收益的挖矿算法与币种，让您赚更多。",
	 commonques11text3:"3、真实挖矿：",
	 commonques11text32:"现在网络上挖矿软件有的是“假”挖矿（积分兑换），有的以高额手续费和门槛限制用户。而源质则是实实在在挖矿的软件，真实调用算力 挖矿且无提现手续费。",
	 commonques11text4:"4、客服帮助：源质浏览器各类社群十分全面，用户遭遇问题随时提交，客服人员会详实记录且尽快解决，让用户有真正的话语权！",
	 community:"社群链接：",
	 Softwareinstr:"软件使用",
	 Softwareinstr1:"一、为什么不能挖矿？",
	 Softwareinstr1text1:"1、被杀软删除部分程序",
	 Softwareinstr1text11:"现在的杀毒软件会将挖矿功能自动判定为风险程序，请点击信任，若仍无法恢复，请关闭杀软后进行操作安装与使用。",
	 Softwareinstr1text2:"2、配置不够",
	 Softwareinstr1text21:"ETH（以太坊）开采需要独立显卡并且至少有4G的显存。",
	 Softwareinstr1text22:"门罗币除集显外，一般显卡都可以正常开采。",
	 Softwareinstr1text23:"如有异常情况，请文字说明并配图，客服会尽快对您的情况进行处理。",
	 Softwareinstr1text24:"一般情况下，不能挖矿、无算力等挖矿异常情况是安装或使用时杀毒软件删除部分文件导致。推荐添加信任或关闭杀软后启动挖矿功能。",
	 Softwareinstr2:"二、为什么源质浏览器被杀毒软件误报？",
	 Softwareinstr2text1:"目前安全软件对所有包含挖矿功能的程序一律上报。",
	 Softwareinstr2text2:"源质浏览器不会对用户造成任何损害，若出现误报误删情况，请添加信任或暂时关闭杀软。",
	 Softwareinstr3:"三、点击开始挖矿后，如何知道程序是否在正常运行？",
	 Softwareinstr3text:"开始挖矿后，浏览器挖矿界面会显示预估收益及算力值，看到数据显示即证明已经正常进行挖矿。如图所示：",
	 Softwareinstr4:"四、源质挖矿挖的是什么币种？用户可以自定义币种么？",
	 Softwareinstr4text1:"源质浏览器会根据用户当前配置，智能选择收益最高的币种。",
	 Softwareinstr4text2:"目前支持以太坊（ETH）与门罗币（XMR）的挖取，日后会根据收益添加其他币种，敬请期待。",
	 Softwareinstr5:"六、在右上角没有找到挖矿功能按钮怎么办？",
	 Softwareinstr5text1:"正常安装后，打开源质浏览器，点击右上角图标",
	 Softwareinstr5text12:"进行注册。",
	 Softwareinstr5text2:"论坛链接：",
	 Softwareinstr5text3:"挖矿插件位置：插件交流区-“重大发现！只需一个插件解决浏览器挖矿所有问题！”",
	 Softwareinstr5text4:"将下载好的挖矿插件拖入浏览器中",
	 Softwareinstr5text5:"点击“添加扩展程序”",
	 Softwareinstr5text6:"挖矿插件添加成功",
	 Softwareinstr5text7:"点击矿锄按钮，即可进入挖矿功能页面。",
	 profiles:"收益与提现",
	 profilestext1:"一、如何查看个人收益，还有怎么提币？",
	 profilestext2:"在挖矿界面左上角“我的钱包”中可看到各币种收益，点击申请提现即可。",
	 profilestext3:"通过电脑挖矿获取的数字货币，及注册等相关活动赠送的ELET都可提现或兑换各类奖品，登录后在个人中心可看到您所有的资产。",
	 profilestext4:"二、源质浏览器的提币现门槛是多少？我能提取到钱包地址吗？",
	 profilestext5:"提币一般1-3个工作日到账，在活动期间内，提币到指定交易所无门槛与手续费。",
	 profilestext6:"其他提币具体事宜，请添加社群，会有专业客服为您详细解答。",
	 profilestext7:"客服QQ：1836228446。",
	 profilestext8:"客服微信：Elementium_ELET",
	 more1:"更多问题",
	 more2:"更多问题，请添加社群，会有人工客服及时为您解决疑惑。"
}	
var faqEN = {
	navtitle1:'FAQ',
	 navtext1:"Download and install",
	 navtext2:"Register",
	 navtext3:"Mining",
	 navtext4:"My Account",
	 navtitle2:"Question type",
	 navtext5:"Common questions",
	 navtext6:"Software Instructions",
	 navtext7:"Income and withdrawal",
	 navtext8:"More questions",
	 instructor:"Browser instructions",
	 instructorText:"The Elementium Browser is the world's first real mining browser, with a minimum of 64-bit operating system and a computer with a discrete graphics card for mining (digital currency acquisition).",
	 download:"Download and install",
	 downloadText1:'Open the Elementium of the future official website',
	 downloadText11:"select the client type to download.",
	 downloadText2:"* Currently supports PC, iOS download, Android and Mac will be online soon.",
	 downloadText3:"After the download is complete, click [One Click Install].",
	 register:"Register",
	 register1:"1. After installation, open the Elementium browser",
	 register11:"and click the icon in the upper right corner to register.",
	 register2:"Or simply click",
	 register21:"to go to the registration page.",
	 register3:"2. Fill in the relevant information according to the prompts, click [Register] after completion.",
	 mining:"Mining",
	 mining1:"1. Enter the mining page",
	 mining2:"Click on the mine icon in the upper right corner to enter the mining page.",
	 mining3:"2. Login",
	 mining4:"Click Login in the upper right corner and enter the registered account and password to log in.",
	 mining5:"3. Calculation pool",
	 mining6:"Click to start mining and you can mine with one button, and you can see the relevant data and income of mining on the left side.",
	 mining7:"Real-time computing power:",
	 mining71:"related to computer performance, better computer performance will get more computing power.",
	 mining8:"Confirmed profit:",
	 mining81:"The profit has been confirmed and the deposit into the accoun.",
	 profit:"Unconfirmed profit:",
	 profitTex:"This is the estimated return of the current calculation power, which is an unsettled income.",
	 protext2:"It will be added to the confirmed earnings after daily billing. (The actual income is subject to the “confirmed revenue”)",
	 protext3:"The list of cryptos below shows the price and increase of the crypto can be mined.",
	 balanceTile:"4. Configure and My Balance",
	 balance1:"When mining begins, you can check the device status in the configuration page.",
	 balance2:"You can adjust the mining efficiency through the setup menu, and you can mine to make money when you are playing games or watch movies.",
	 balance3:"In My Balance page, you can see all the cryptos, profit, and last profit confirmed time of current device.",
	 myaccount:"My Account",
	 enteraccount:"1. Enter My Account",
	 enteraccounttext1:"In the login state, click the icon in the upper left corner",
	 enteraccounttext12:"to enter the personal center. As shown below:",
	 myassets:"2. My assets and my devices",
	 myassettext1:"Enter my personal center to see my assets and my equipment.",
	 myassettext2:"My assets",
	 myassettext3:"Cryptos: The currency type of all proceeds.",
	 myassettext4:"Accumulative Total Earnings: The sum of all devices mining profit of the current account.",
	 myassettext5:"Available: The profit that can be withdrawn.",
	 myassettext6:"Frozen: The profit that is in withdrawal process.",
	 myassettext7:"Withdrawn: The profit of the successful extracted.",
	 myassettext8:"My devices: One account of Elementium Browser can use multiple devices to mine.",
	 myassettext9:"The specific status of all mining device can be seen in My device page and can be categorized.",
	 withdrawal:"Tips for withdrawal:",
	 withdrawaltext1:"1. The withdrawal is generally done in 1-3 business days.",
	 withdrawaltext2:"2. During the promotion period, there is no threshold and fee for the withdrawal of the crypto to the designated exchanges.",
	 withdrawaltext3:"If you have any questions, please add customer service QQ: 1836228446.",
	 questionType:"Elementium browser questions type",
	 commonques:"Common questions",
	 commonques1:"1. The difference between the Elementium browser and the other browsers?",
	 commonques1text:"Different from other browsers, the Elementium browser is the world's first real mining browser. In addition to the basic speed browsing feature, it also has intelligent mining and blockchain high-quality information collection.",
	 commonques2:"2. What is mining? What will you get?",
	 commonques1text1:"Mining can be understood as the use of CPU, GPU and other resources in the computer for digital currency acquisition, the better the performance, the higher the mining revenue.",
	 commonques1text2:"The acquired digital currency can be converted into money, and the Elementium browser provides you with one-click intelligent mining.",
	 commonques3:"3. What kind of computer can mine with the Elementium browser?",
	 commonques3text:"The Elementium browser uses a mining function with a minimum of 64-bit operating system and a separate graphics card.",
	 commonques4:"4. What kinds OS can use the Elementium browser?",
	 commonques4text:"The Elementium browser is currently open for download on the PC and iOS, and Android and Mac will be online soon.",
	 commonques5:"5. What are the current support for mining cryptos?",
	 commonques5text:"The Elementium browser supports Ethereum (ETH) and Monroe Coin (XMR), and will add new cryptos based on the efficiency and revenue of the mining.",
	 commonques6:"6. How much can I earn by mining every day?",
	 commonques6text:"The income earned by mining is related to your computer performance and crypto price. Generally, common performance computer hangs up about 4 yuan per day. The higher the performance, the better the income. A rise in the price of the cryptos will also increase your income. In addition, ELET rewards are also available daily using the Elementium browser and participating in related activities.",
	 commonques7:"7. What is ELET?",
	 commonques7text:"ELET as a decentralized Blockchain digital asset is the Tokens issued based on Elementium Future Ecology.",
	 commonques8:"8. Use of ELET",
	 commonques8text:"ELET is a stable and ever-increasing digital asset that can be purchased and sold on a variety of exchanges and used as a payment tool in a variety of scenarios, including Elementium browsers and Elementium OS.",
	 commonques9:"9. How to obtain ELET",
	 commonques9text:"ELET can now be purchased through an exchange or mined using a Elementium browser and participate in related promotions.",
	 commonques10:"10. What are the official exchange groups of ELET?",
	 
	 commonques10text3:"WeChat group: Elementium_ELET",
	 commonques11:"11. Why do many miners use Elementium browsers to mine?",
	 commonques11text1:"1. Easy to use: just register and log in, you can carry out one-click mining without any complicated settings.",
	 commonques11text2:"2. The best returns: Elementium intelligent mining according to the machine performance and the owner's needs, automatically select the highest income mining algorithm and currency, so that you earn more.",
	 commonques11text3:"3. Real mining:",
	 commonques11text32:"Now there are lot's of 'fake' mining software (point redemption) on the network, and some limit users with high fees and thresholds. The Elementium is the software that is actually mining, and the real calculations,and there is no withdrawal fee.",
	 commonques11text4:"4. Customer service help: Elementium browser various communities are very comprehensive, users encounter problems at any time to submit, customer service staff will be detailed records and resolve as soon as possible, so that users have a real right to speak!",
	 community:"Community link:",
	 Softwareinstr:"Software instructions",
	 Softwareinstr1:"1. Why can't mine?",
	 Softwareinstr1text1:"1. Deleted by some anti-virus software",
	 Softwareinstr1text11:"Now mining is not allowed by some anti-virus software, and they will automatically determine the mining function as a risk program, please click trust, if it still can't be recovered, please close the software and reinstall the browser.",
	 Softwareinstr1text2:"2. Not enough performance",
	 Softwareinstr1text21:"ETH (Ethereum) mining requires a separate graphics card and at least 4G of video memory.",
	 Softwareinstr1text22:"For the Monroe(XMR), the general graphics card and CPU can be mined normally.",
	 Softwareinstr1text23:"If there are any abnormalities, please contact us, and the customer service will handle your situation as soon as possible.",
	 Softwareinstr1text24:"Usually, mining error such as mining start failure, no caculate power, etc. are caused by the removal of some files by the anti-virus software during installation or use. It is recommended to add trust or close killing before start mining.",
	 Softwareinstr2:"2. Why is the Elementium browser being misreported by anti-virus software?",
	 Softwareinstr2text1:"Currently, security software reports all procedures that include mining functions.",
	 Softwareinstr2text2:"The Elementium browser will not cause any damage to the user. If there is a false positive or false deletion, please add trust or temporarily disable the soft.",
	 Softwareinstr3:"4. After clicking to start mining, how can you I if the program is running normally?",
	 Softwareinstr3text:"After starting mining, the browser mining page will display the estimated profit and the Hashrate value. When the data is displayed, it indicates that the mining has been carried out normally. as the picture shows:",
	 Softwareinstr4:"5. What cryptos is the Elementium usually mining? Can users change the crypto?",
	 Softwareinstr4text1:"The Elementium browser intelligently selects the crypto with the highest profit based on the user's current performance.",
	 Softwareinstr4text2:"Currently, Ethereum (ETH) and Monroe Coin (XMR) are supported for excavation. In the future, other cryptos will be added according to the profit, so stay tuned.",
	 Softwareinstr5:"6. What can I do if I can't find the mining function button in the upper right corner?",
	 Softwareinstr5text1:"After normal installation, open the Elementium browser and click the icon in the upper right corner",
	 Softwareinstr5text12:" to register.",
	 Softwareinstr5text2:"Forum link: ",
	 Softwareinstr5text3:'Mining plugin location: plugin exchange area - "Major discovery! One plugin, can solve all mining problems!"',
	 Softwareinstr5text4:"Drag the downloaded mining plugin into the browser",
	 Softwareinstr5text5:'Click on "Add Extension"',
	 Softwareinstr5text6:"Miner extension added ",
	 Softwareinstr5text7:"Click the mine button to enter the Miner page.",
	 profiles:"Income and withdrawal",
	 profilestext1:"1. How to check the balance, and how to withdrawal?",
	 profilestext2:'Click the "Balance" button in the upper left corner of the mining page, you can see the profit of each crypto. Click "Withdrawal" button to extract.',
	 profilestext3:'Digital crypto obtained through computer mining, and ELET donated by related activities such as registration can be used to withdraw or redeem various prizes. After logged in, you can see all your assets in the "My Asset" page.',
	 profilestext4:"2. What is the threshold? Can I withdraw to the wallet address?",
	 profilestext5:"The withdrawal is usually checked in 1-3 business days. During the promotion period, there is no threshold and withdrawal fee for the designated exchange.",
	 profilestext6:"For more details, please join the community, there will be professional customer service for you.",
	 profilestext7:"Customer service QQ: 1836228446.",
	 profilestext8:"Customer Service WeChat: Elementium_ELET",
	 more1:"More questions",
	 more2:"For more questions, please join the community, there will be professional customer service for you."
}
// toolbar = toolbarch;
// home = homech;
// Elementium = Elementiumch;
// faq = faqCN;
// changeLanguage();
// var mark = true;
function setImgUrlEN(){
	$('#whitepaper').attr('href','源质未来白皮书v1.21.pdf');
	$('#lod1Img').attr('src','images/19.png');
	$('#regImg1').attr('src','images/02.jpg');
	$('#regImg2').attr('src','images/03.jpg');
	$('#regImg3').attr('src','images/04.jpg');
	$('#minerImg1').attr('src','images/05.jpg');
	$('#minerImg2').attr('src','images/06.jpg');
	$('#minerImg3').attr('src','images/07.jpg');
	$('#minerImg4').attr('src','images/08.png');
	$('#minerImg5').attr('src','images/09.png');
	$('#minerImg6').attr('src','images/10.png');
	$('#minerImg7').attr('src','images/11.png');
	$('#personalImg1').attr('src','images/12.png');
	$('#personalImg2').attr('src','images/13.jpg');
	$('#useImg1').attr('src','images/14.png');
	$('#useImg2').attr('src','images/16.png');
	$('#useImg3').attr('src','images/17.png');
	$('#useImg4').attr('src','images/18.png');
}
function setImgUrlCN(){
	$('#whitepaper').attr('href','White_Paper_for_Elementium_Future_Project(v.1.20).pdf');
	$('#lod1Img').attr('src','images/19_en.jpg');
	$('#regImg1').attr('src','images/02_en.jpg');
	$('#regImg2').attr('src','images/03_en.jpg');
	$('#regImg3').attr('src','images/04_en.jpg');
	$('#minerImg1').attr('src','images/05_en.jpg');
	$('#minerImg2').attr('src','images/06_en.jpg');
	$('#minerImg3').attr('src','images/07_en.jpg');
	$('#minerImg4').attr('src','images/08_en.jpg');
	$('#minerImg5').attr('src','images/09_en.jpg');
	$('#minerImg6').attr('src','images/10_en.jpg');
	$('#minerImg7').attr('src','images/11_en.jpg');
	$('#personalImg1').attr('src','images/02_en.jpg');
	$('#personalImg2').attr('src','images/13_en.jpg');
	$('#useImg1').attr('src','images/14_en.jpg');
	$('#useImg2').attr('src','images/16_en.jpg');
	$('#useImg3').attr('src','images/17_en.jpg');
	$('#useImg4').attr('src','images/18_en.jpg');
}
console.log(localStorage.getItem("lang"))

if (localStorage.getItem("lang")=="en") {
	toolbar = toolbaren;
	home = homeen;
	Elementium = Elementiumen;
	faq = faqEN;
	setImgUrlCN();
	changeLanguage();
}else{
	toolbar = toolbarch;
	home = homech;
	Elementium = Elementiumch;
	faq = faqCN;
	
	setImgUrlEN();
	changeLanguage();
}
$(".Language").click(function(){
	if (localStorage.getItem("lang")=="en") {
		toolbar = toolbarch;
		home = homech;
		Elementium = Elementiumch;
		faq = faqCN;
		setImgUrlEN();
		localStorage.setItem("lang","cn");
		changeLanguage();
		console.log(1)
	}else{
		
		localStorage.setItem("lang","en");
		toolbar = toolbaren;
		home = homeen;
		faq = faqEN;
		Elementium = Elementiumen;
		
		setImgUrlCN();

		changeLanguage();
		
		console.log(2)
	}
		// if (mark) {
		// 	localStorage.setItem("lang","en");
		// 	console.log(1)
		// 	toolbar = toolbaren;
		// 	home = homeen;
		// 	Elementium = Elementiumen;
		// 	changeLanguage();
		// 	mark = false;
		// }else{
		// 	localStorage.setItem("lang","cn");
		// 	toolbar = toolbarch;
		// 	home = homech;
		// 	console.log(2)
		// 	Elementium = Elementiumch;
		// 	mark =true;
		// 	changeLanguage();
		// }
			
	})
function changeLanguage(){
	$('*[pagetext]').each(function(i,e){
		var elem = $(e);
		var _text = elem.attr('pagetext');
		elem.text(toolbar[_text]);
		elem.text(home[_text]);
		elem.text(Elementium[_text]);
	});
	$('*[pagetitle]').each(function(i,e){
		var elem = $(e);
		var _text = elem.attr('pagetitle');
		elem.attr("title",toolbar[_text]);
		elem.attr("title",home[_text]);
		elem.attr("title",Elementium[_text]);
	});
	$('*[pageholder]').each(function(i,e){
		var elem = $(e);
		var _text = elem.attr('pageholder');
		elem.attr("placeholder",toolbar[_text]);
		elem.attr("placeholder",home[_text]);
		elem.attr("placeholder",Elementium[_text]);
	});
	$('*[pagefaq]').each(function(i,e){
		var elem = $(e);
		var _text = elem.attr('pagefaq');
		elem.text(faq[_text]);	
	});
}
	
		
